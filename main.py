import json
import os


class Search:

    def search(self, directory):
        """Loops through directory of annotated jobads in json format."""

        for filename in os.listdir(directory):
            if filename != '.git':
                filename = os.path.join(directory, filename)
                self._read_file(filename)

    def _read_file(self, filename):
        """Finds job ads containing more than one occupation.
        
        Loops through the json formatted annotated job ad.
        Adds the concept_id of every annotation of type "occupation-name" to set.
        Also adds the concept_id of the meta annotation "occupation" to set.
        If the set is longer than 1, the job ad is assumed to contain more than
        one annotated occupation-name. The filename is than added to a text file."""

        ids = set()
        with open(filename, 'r', encoding='utf-8') as json_file:
            job_ad = json.load(json_file)

            try:
                ids.add(job_ad["meta"]["job_ad"]["occupation"]["concept_id"])
            except KeyError:
                pass

            for annotation in job_ad["annotations"]:
                if annotation["type"] == 'occupation-name':
                    ids.add(annotation["concept-id"])
                
            if len(ids) > 1:
                self._write(filename)

    def _write(self, filename):
        """Writes filename of job ads containing more than one occupation-name to txt file."""

        target_file = 'occupation-names_filenames.txt'
        with open(target_file, 'a') as fhand:
            fhand.write(filename+ '\n')

search = Search()
search.search('mentor-api-prod')